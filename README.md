datalife_stock_voucher
======================

The stock_voucher module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_voucher/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_voucher)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
