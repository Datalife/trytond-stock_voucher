===================================
Stock Voucher Planned Date Scenario
===================================

Imports::

    >>> import datetime
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.stock_voucher.tests.tools import create_template
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> date = datetime.date(2018, 1, 1)
    >>> date2 = datetime.date(2018, 10, 1)
    >>> date3 = datetime.date(2018, 10, 2)

Install stock_voucher::

    >>> config = activate_modules('stock_voucher')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> lost_loc, = Location.find([('type', '=', 'lost_found')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> internal_loc = Location(name='Internal', type='storage')
    >>> internal_loc.save()
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> warehouse_loc2, = warehouse_loc.duplicate()

Create stock configuration::

    >>> Configuration = Model.get('stock.configuration')
    >>> conf = Configuration(1)
    >>> template = create_template(company, 'Product 1')
    >>> product, = template.products
    >>> conf.voucher_product = product
    >>> conf.voucher_comment = '${company_name} ${issued_by_name} ${number} ${reference} ${date} ${product_name}'
    >>> conf.save()

Create parties and addresses::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> emitter = Party(name='Emitter')
    >>> emitter.save()
    >>> issued_by = Party(name='Issuer')
    >>> issued_by.save()
    >>> carrier = Party(name='Receiver')
    >>> carrier.save()
    >>> address = carrier.addresses[0]
    >>> address.name = 'Address Receiver'
    >>> address.save()
    >>> lost_loc.address = address
    >>> lost_loc.save()
    >>> warehouse_loc.service_party = issued_by
    >>> warehouse_loc.save()

Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> carrier = Carrier(party=carrier)
    >>> carrier_product, = create_template(company, 'Carrier service', 'service').products
    >>> carrier.carrier_product = carrier_product
    >>> carrier.save()

Create voucher::

    >>> Voucher = Model.get('stock.voucher')
    >>> voucher = Voucher()
    >>> voucher.company = company
    >>> voucher.issued_by = issued_by
    >>> voucher.carrier = carrier
    >>> voucher.date = date
    >>> voucher.reference = 'R1'
    >>> voucher.quantity = 10
    >>> voucher.issuing_address.rec_name
    'Issuer'
    >>> voucher.emitter = emitter
    >>> voucher.issuing_warehouse = warehouse_loc
    >>> voucher.warehouse = warehouse_loc2
    >>> voucher.save()
    >>> voucher.number
    '1'
    >>> voucher.quantity
    10.0
    >>> voucher.product == conf.voucher_product
    True
    >>> voucher.state
    'draft'
    >>> voucher.click('wait')

Create voucher::

    >>> voucher2 = Voucher()
    >>> voucher2.company = company
    >>> voucher2.issued_by = issued_by
    >>> voucher2.carrier = carrier
    >>> voucher2.date = date
    >>> voucher2.reference = 'R2'
    >>> voucher2.quantity = 10
    >>> voucher2.emitter = emitter
    >>> voucher2.issuing_warehouse = warehouse_loc
    >>> voucher2.warehouse = warehouse_loc2
    >>> voucher2.save()
    >>> voucher2.number
    '2'
    >>> voucher2.click('wait')

Define planned date on 2 vouchers and check results::

    >>> define_planned_date = Wizard('stock.voucher.planned_date.define', [voucher, voucher2])
    >>> define_planned_date.form.date = date2
    >>> define_planned_date.form.comment = 'Comment'
    >>> define_planned_date.execute('define')
    >>> voucher.planned_date == date2
    True
    >>> voucher.planned_date == voucher2.planned_date
    True
    >>> len(voucher.planned_dates)
    0

Check planned_date field when voucher state return to 'process'::
    
    >>> voucher.click('process')
    >>> done_voucher = Wizard('stock.voucher.done', [voucher])
    >>> done_voucher.form.received_by = carrier.party
    >>> done_voucher.form.received_date = today
    >>> done_voucher.form.to_location = lost_loc
    >>> done_voucher.execute('shipment')
    >>> done_voucher.execute('done')
    >>> voucher.state
    'recovery'
    >>> len(voucher.shipments_internal)
    1
    >>> shipment, = voucher.shipments_internal
    >>> shipment.click('wait')
    >>> _ = shipment.click('assign_wizard')
    >>> shipment.click('done')
    >>> voucher.reload()
    >>> voucher.state
    'done'
    >>> voucher.planned_date == date2
    True
    >>> voucher.click('undo')
    >>> not voucher.planned_date
    True
    >>> len(voucher.planned_dates)
    1
    >>> voucher.planned_date = date
    >>> voucher.save()
    >>> define_planned_date = Wizard('stock.voucher.planned_date.define', [voucher, voucher2])
    >>> define_planned_date.form.date = date3
    >>> define_planned_date.form.comment = 'Comment 2'
    >>> define_planned_date.execute('define')
    >>> voucher.reload()
    >>> voucher2.reload()
    >>> voucher.planned_date == date3
    True
    >>> voucher.planned_date == voucher2.planned_date
    True
    >>> len(voucher.planned_dates)
    2