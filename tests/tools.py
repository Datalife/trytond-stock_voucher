# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from proteus import Model


def create_template(company, name, type_='goods'):
    ProductUom = Model.get('product.uom')
    ProductTemplate = Model.get('product.template')

    unit, = ProductUom.find([('name', '=', 'Unit')])

    template = ProductTemplate()
    template.name = name
    template.type = type_
    template.default_uom = unit
    template.list_price = Decimal('30')
    template.save()
    return template
