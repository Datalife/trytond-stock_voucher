# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from datetime import datetime
from string import Template
from itertools import groupby
from trytond.model import Workflow, ModelView, ModelSQL, fields, Model
from trytond.pyson import Eval, If
from trytond.pool import Pool
from trytond.wizard import (Wizard, StateView, Button, StateTransition,
    StateReport)
from trytond.transaction import Transaction
from trytond.modules.company import CompanyReport
from dateutil.relativedelta import relativedelta
from trytond.modules.sale.sale import ModifyHeaderStateView
from sql import Null
from sql.aggregate import Max


STATES = {'readonly': (Eval('state') != 'draft')}


class Voucher(Workflow, ModelSQL, ModelView):
    '''Stock voucher'''
    __name__ = 'stock.voucher'
    _rec_name = 'number'

    company = fields.Many2One('company.company', 'Company', required=True,
        states=STATES, depends=['state'])
    issued_by = fields.Many2One('party.party', 'Issued by', required=True,
        states=STATES, depends=['state'])
    issuing_address = fields.Many2One('party.address', 'Issuing address',
        domain=[
            ('party', '=', Eval('issued_by'))
        ],
        states=STATES, depends=['state', 'issued_by'])
    issuing_warehouse = fields.Many2One('stock.location', 'Issuing warehouse',
        domain=[
            ('type', '=', 'warehouse'),
            ('service_party', '=', Eval('issued_by'))],
        states=STATES, depends=['state', 'issued_by'])
    warehouse = fields.Many2One('stock.location', 'Warehouse',
        domain=[('type', '=', 'warehouse')], states={
            'readonly': STATES['readonly'],
            'required': Eval('state').in_(['waiting', 'processing', 'done'])},
        depends=['state'])
    reference = fields.Char("Reference", select=True,
        states=STATES, depends=['state'])
    number = fields.Char('Number', select=True, readonly=True)
    date = fields.Date('Date', required=True,
        states={
            'readonly': Eval('state').in_(['cancelled', 'processing', 'done']),
            },
        depends=['state'])
    maturity_date = fields.Date('Maturity Date',
        states={
            'readonly': Eval('state').in_(['cancelled', 'done']),
            },
        depends=['state'])
    planned_date = fields.Date('Planned Date')
    planned_dates = fields.One2Many('stock.voucher.planned_date', 'voucher',
        'Planned Dates', order=[('create_date', 'DESC')])
    shipments_internal = fields.Function(
        fields.One2Many('stock.shipment.internal', None, 'Internal Shipments',
            states={'readonly': Eval('state') != 'processing'},
            depends=['state']),
        'get_shipments', setter='set_shipments')
    shipments_out = fields.Function(
        fields.One2Many('stock.shipment.out', None, 'Customer Shipments',
            states={'readonly': Eval('state') != 'processing'},
            depends=['state']),
        'get_shipments', setter='set_shipments')
    product = fields.Many2One('product.product', 'Product',
        states={
            'readonly': Eval('state') != 'draft',
            'required': Eval('state').in_(['waiting', 'processing', 'done'])
        }, depends=['state'])
    quantity = fields.Float('Quantity',
        domain=[('quantity', '>', 0)],
        digits=(16, Eval('unit_digits', 2)),
        states={
            'readonly': Eval('state') != 'draft',
            'required': Eval('state').in_(['waiting', 'processing', 'done'])
        },
        depends=['unit_digits', 'state'])
    product_uom_category = fields.Function(
        fields.Many2One('product.uom.category', 'Product Uom Category'),
        'on_change_with_product_uom_category')
    uom = fields.Many2One('product.uom', 'UOM', required=True, states=STATES,
        domain=[
            ('category', '=', Eval('product_uom_category')),
            ],
        depends=['state', 'product_uom_category'])
    unit_digits = fields.Function(fields.Integer('Unit Digits'),
        'on_change_with_unit_digits')
    receiver_moves = fields.Function(
        fields.One2Many('stock.move', 'origin', 'Receiver moves',
            states={'readonly': Eval('state') != 'processing'},
            depends=['state']),
        'get_receiver_moves', setter='set_receiver_moves')
    external_picking = fields.Char('External picking',
        states=STATES, depends=['state'])
    external_picking_date = fields.Date('External Picking Date',
        states=STATES, depends=['state'])
    company_property = fields.Boolean('Company property',
        states=STATES, depends=['state'])
    carrier = fields.Many2One('carrier', 'Carrier',
        states={
            'readonly': Eval('state').in_(['cancelled', 'processing', 'done']),
        }, depends=['state'])
    emitter = fields.Many2One('party.party', 'Emitter', required=True,
        states=STATES, depends=['state'])
    forced = fields.Boolean('Forced',
        states=STATES, depends=['state'])
    managed_by = fields.Char('Managed by',
        states=STATES, depends=['state'])
    notify_date = fields.Date('Notify Date',
        states={
            'readonly': Eval('state').in_(['cancelled', 'done']),
            },
        depends=['state'])
    received_by = fields.Many2One('party.party', 'Received by', readonly=True,
        states={
            'required': Eval('state') == 'done',
        },
        depends=['state'])
    received_date = fields.Date('Received Date', readonly=True,
        states={
            'required': Eval('state') == 'done'
        },
        depends=['state'])
    moves = fields.Many2Many('stock.voucher-stock.move', 'voucher',
        'move', 'Moves', readonly=True)
    stock_moves = fields.One2Many('stock.move', 'origin', 'Stock moves',
        readonly=True)
    external_picking_2 = fields.Char('Secondary External picking',
        states=STATES, depends=['state'])
    external_picking_date_2 = fields.Date('Secondary External Picking Date',
        states=STATES, depends=['state'])
    holders = fields.Many2Many('stock.voucher.holder',
        'voucher', 'holder', 'Holders', states={
            'readonly': Eval('state').in_(['cancelled', 'done'])
        }, order=[('create_date', 'ASC')],
        depends=['state'])
    holders_rel = fields.One2Many('stock.voucher.holder', 'voucher',
        'Holders relation', readonly=True)
    holder = fields.Function(fields.Many2One('party.party', 'Holder'),
        'get_holder', searcher='search_holder')
    holder_date = fields.Function(fields.Date('Holder date'),
        'get_holder_date')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting', 'Waiting'),
        ('processing', 'Processing'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled'),
        ('recovery', 'In recovery'),
        ], 'State', readonly=True)
    count = fields.Function(fields.Integer('Count'), 'get_count')
    sales = fields.Function(
        fields.One2Many('sale.sale', None, 'Sales',
            states={'readonly': Eval('state') != 'processing'},
            depends=['state']),
        'get_sales', setter='set_sales')
    comment = fields.Text('Comment')
    receiver_date = fields.Function(fields.Date('Receiver move date'),
        'get_move_data')
    receiver_state = fields.Function(
        fields.Selection('_get_move_states', 'Receiver move State'),
        'get_move_data')
    receiver_shipment = fields.Function(
        fields.Reference('Receiver Shipment',
            selection='_get_shipment_origins'),
        'get_move_data')
    out_date = fields.Function(fields.Date('Move out date'),
        'get_move_data')
    out_state = fields.Function(
        fields.Selection('_get_move_states', 'Move out State'),
        'get_move_data')
    out_shipment = fields.Function(
        fields.Reference('Move out Shipment',
            selection='_get_shipment_origins'),
        'get_move_data')
    write_uid_planned = fields.Many2One('res.user', 'Write User Planned date',
        readonly=True)
    write_date_planned = fields.Timestamp('Write Date Planned date',
        readonly=True)
    returned = fields.Boolean('Returned to Customer/Emitter', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Voucher, cls).__setup__()
        cls._transitions |= set((
                ('draft', 'cancelled'),
                ('cancelled', 'draft'),
                ('draft', 'waiting'),
                ('waiting', 'draft'),
                ('waiting', 'cancelled'),
                ('waiting', 'processing'),
                ('processing', 'done'),
                ('done', 'processing'),
                ('processing', 'recovery'),
                ('recovery', 'done'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': ~Eval('state').in_(['waiting', 'draft']),
                    'depends': ['state'],
                    },
                'draft': {
                    'invisible': ~Eval('state').in_(['waiting', 'cancelled']),
                    'icon': If(Eval('state') == 'waiting',
                        'tryton-back', 'tryton-undo'),
                    'depends': ['state'],
                    },
                'wait': {
                    'invisible': Eval('state') != 'draft',
                    'icon': If(Eval('state') == 'processing',
                        'tryton-back',
                            If(Eval('state') == 'waiting', 'tryton-undo',
                            'tryton-forward')),
                    'depends': ['state'],
                    },
                'process': {
                    'invisible': Eval('state') != 'waiting',
                    'icon': 'tryton-forward',
                    'depends': ['state'],
                    },
                'done_try': {
                    'invisible': Eval('state') != 'processing',
                    'depends': ['state'],
                    },
                'done': {
                    'invisible': Eval('state') != 'processing',
                    'depends': ['state'],
                    },
                'unprocess': {
                    'invisible': Eval('state') != 'processing',
                    'icon': 'tryton-back',
                    'depends': ['state']
                    },
                'undo': {
                    'invisible': Eval('state') != 'done',
                    'icon': 'tryton-back',
                    'depends': ['state']
                    },
                'unrecover': {
                    'invisible': Eval('state') != 'recovery',
                    'icon': 'tryton-back',
                    'depends': ['state']
                }
            })
        cls._error_messages.update({
            'not_to_location': 'The issuing address "%s" has no location.',
            'not_from_location': 'The party associated with the carrier "%s" '
                'has no location.',
            'validate_unique': 'Key must be unique: \n%s',
            'move_not_cancelled': 'Move "%s" must be cancelled.',
            'shipment_not_cancelled': 'Shipment "%s" must be cancelled.',
            'sale_not_cancelled': 'Sale "%s" must be cancelled.'
        })

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()

        super().__register__(module_name)

        # Migration from 5.6: rename state cancel to cancelled
        cursor.execute(*sql_table.update(
                [sql_table.state], ['cancelled'],
                where=sql_table.state == 'cancel'))

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Config = pool.get('stock.configuration')

        vlist = [x.copy() for x in vlist]
        config = Config(1)
        default_company = cls.default_company()
        for values in vlist:
            if values.get('number') is None:
                values['number'] = config.get_multivalue(
                    'voucher_sequence',
                    company=values.get('company', default_company)).get()
        vouchers = super(Voucher, cls).create(vlist)
        return vouchers

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        args = []
        for records, values in zip(actions, actions):
            if 'planned_date' in values:
                values['write_date_planned'] = datetime.now()
                values['write_uid_planned'] = Transaction().user
            args.extend((records, values))
        super().write(*args)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('number', None)
        default.setdefault('moves', None)
        default.setdefault('receiver_moves', None)
        default.setdefault('holders', None)
        default.setdefault('holders_rel', None)
        default.setdefault('planned_dates', None)
        default.setdefault('write_date_planned', None)
        default.setdefault('write_uid_planned', None)
        return super(Voucher, cls).copy(records, default=default)

    @staticmethod
    def default_product():
        pool = Pool()
        Config = pool.get('stock.configuration')
        config = Config(1)
        return config.voucher_product.id if config.voucher_product else None

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company', None)

    def get_holder(self, name=None):
        if self.holders:
            return self.holders[-1].id

    def get_holder_date(self, name=None):
        if self.holders_rel:
            return self.holders_rel[-1].date

    @classmethod
    def search_holder(cls, name, clause):
        VoucherHolder = Pool().get('stock.voucher.holder')
        vholder = VoucherHolder.__table__()
        cursor = Transaction().connection.cursor()

        cursor.execute(*vholder.select(
            Max(vholder.id),
            vholder.voucher,
            group_by=vholder.voucher)
        )
        holder_ids = [r[0] for r in cursor.fetchall()]

        nested = clause[0].lstrip(name)
        if nested:
            holder_clause = clause
        else:
            if isinstance(clause[2], str):
                target = 'rec_name'
            else:
                target = 'id'
            holder_clause = ('holder.' + target,) + tuple(clause[1:])
        return [
            ('holders_rel', 'where', [
                ('id', 'in', holder_ids),
                holder_clause]
            )]

    def get_shipments(self, name=None):
        _, shipment_name = name.split('_')
        shipments = [move.shipment.id for move in self.stock_moves
            if move.shipment
            and move.shipment.__name__ == ('stock.shipment.%s' % shipment_name)
            and move not in self.receiver_moves]
        shipments.extend([move.shipment.id for move in self.moves
            if move.shipment
            and move.shipment.__name__ == ('stock.shipment.%s' % shipment_name)
        ])
        return list(set(shipments))

    @classmethod
    def set_shipments(cls, records, name, value, check_cancelled=True):
        pool = Pool()
        Voucher_Move = pool.get('stock.voucher-stock.move')
        Move = pool.get('stock.move')
        shipment_model = cls._fields[name].model_name
        Shipment = pool.get(shipment_model)

        voucher_move_rels = None
        assert len(records) == 1
        voucher = records[0]
        for value_changes in value:
            action, shipment_values = value_changes
            if action == 'delete':
                shipments = Shipment.browse(shipment_values)
                moves = [m for s in shipments for m in s.moves
                    if voucher in m.vouchers]
                moves_to_del = [m for s in shipments for m in s.moves
                    if isinstance(m.origin, cls) and m.origin == voucher]
                voucher_move_rels = Voucher_Move.search([
                    ('voucher.id', 'in', list(map(int, records))),
                    ('move.id', 'in', list(map(int, moves + moves_to_del)))])
                for shipment in shipments:
                    if check_cancelled and shipment.state != 'cancel':
                        cls.raise_user_error('shipment_not_cancelled',
                            shipment.rec_name)
                for move in moves:
                    if move.origin and isinstance(move.origin, cls):
                        move.origin = None
                Move.save(moves)
                if moves_to_del:
                    Move.delete(list(set(moves_to_del)))

        if voucher_move_rels:
            qty2remove = {}
            for move_voucher in voucher_move_rels:
                qty2remove.setdefault(move_voucher.move, 0)
                qty2remove[move_voucher.move] += move_voucher.voucher.quantity
            to_cancel = [m for m in qty2remove.keys() if m.state == 'done']
            if to_cancel:
                with Transaction().set_context(
                        check_origin=False,
                        check_shipment=False):
                    Move.cancel(to_cancel)
                    Move.draft(to_cancel)
            moves = Move.browse(list(qty2remove.keys()))
            for move in moves:
                move.quantity -= qty2remove[move]
            Move.save(moves)
            to_del = [m for m in moves if not m.quantity]
            if to_del:
                Move.delete(to_del)
            if to_cancel:
                Move.do(Move.browse(to_cancel))
            Voucher_Move.delete(voucher_move_rels)

    def get_sales(self, name=None):
        return list(set(move.origin.sale.id for move in self.moves
            if move.origin and move.origin.__name__ == 'sale.line')) or []

    @classmethod
    def set_sales(cls, records, name, value, check_cancelled=True):
        pool = Pool()
        Sale = pool.get('sale.sale')
        Move = pool.get('stock.move')
        Voucher_Move = pool.get('stock.voucher-stock.move')

        assert len(records) == 1
        voucher = records[0]
        qty2remove = {}
        for value_changes in value:
            action, sale_values = value_changes
            if action == 'delete':
                sales = Sale.browse(sale_values)
                for sale in sales:
                    if check_cancelled and sale.state != 'cancel':
                        cls.raise_user_error('sale_not_cancelled',
                            sale.rec_name)
                moves = [move for move in voucher.moves
                    if move.origin
                    and move.origin
                    and move.origin.__name__ == 'sale.line'
                    and move.origin.sale.id in list(map(int, sales))
                ]
                Move.save(moves)
                voucher_move_rels = Voucher_Move.search([
                    ('move.id', 'in', list(map(int, moves))),
                    ('voucher.id', '=', voucher.id)])
                for move_voucher in voucher_move_rels:
                    qty2remove.setdefault(move_voucher.move.origin, 0)
                    qty2remove[move_voucher.move.origin] += \
                        move_voucher.voucher.quantity

                to_draft = []
                for sale in sales:
                    if sale.state in ('draft', 'cancel'):
                        continue
                    elif sale.state in ('quotation', 'confirmed'):
                        to_draft.append(sale)
                    elif sale.state == 'processing':
                        if sale.shipment_method != 'manual':
                            sale.shipment_method = 'manual'
                            sale.save()
                        Sale.unprocess([sale])
                if to_draft:
                    Sale.draft(to_draft)

                cls.set_shipments(records, 'shipments_out',
                    [('delete', [sh.id for s in sales for sh in s.shipments])],
                    check_cancelled=check_cancelled)

                for line in [l for s in sales for l in s.lines]:
                    line.quantity -= qty2remove[line]
                    line.save()

    def get_receiver_moves(self, name=None):
        from_location, to_location = self._get_internal_shipment_locations()
        if self.stock_moves:
            return [move.id for move in self.stock_moves
                if move.from_location == from_location
                and move.to_location == to_location
            ]
        return []

    @classmethod
    def set_receiver_moves(cls, records, name, value):
        pool = Pool()
        Move = pool.get('stock.move')

        for value_changes in value:
            action, move_values = value_changes
            if action == 'delete':
                moves = Move.browse(move_values)
                for move in moves:
                    if move.state != 'cancel':
                        cls.raise_user_error('move_not_cancelled',
                            move.rec_name)
                for move in moves:
                    move.origin = None
                Move.save(moves)

    @classmethod
    @Workflow.transition('draft')
    def draft(cls, vouchers):
        for voucher in vouchers:
            try:
                holders_no_emitter = list(voucher.holders)
                holders_no_emitter.remove(voucher.emitter)
                voucher.holders = holders_no_emitter
            except ValueError:
                pass
        if vouchers:
            cls.save(vouchers)

    @classmethod
    @Workflow.transition('waiting')
    def wait(cls, vouchers):
        pool = Pool()
        Holder = pool.get('stock.voucher.holder')

        cls.check_unique_restriction(vouchers, [
            'issued_by', 'external_picking', 'external_picking_date'])
        cls.check_unique_restriction(vouchers, [
            'issued_by', 'external_picking_2', 'external_picking_date_2'])

        holders = [Holder(holder=voucher.emitter,
            voucher=voucher) for voucher in vouchers]
        if holders:
            Holder.save(holders)

    @classmethod
    @Workflow.transition('cancelled')
    def cancel(cls, vouchers):
        pass

    @classmethod
    @ModelView.button
    def unprocess(cls, records):
        Shipment = Pool().get('stock.shipment.internal')

        to_undo = [r for r in records if r.state == 'processing']
        shipments = set([move.shipment for r in to_undo
            for move in r.receiver_moves if move.shipment])
        if shipments:
            with Transaction().set_context(check_origin=False):
                Shipment.cancel(list(shipments))
        if to_undo:
            cls.write(records, {'state': 'waiting'})

    @classmethod
    @ModelView.button
    def undo(cls, records):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        ShipmentOut = pool.get('stock.shipment.out')
        Sale = pool.get('sale.sale')

        internal_shipments, out_shipments = [], []
        for record in records:
            if record.shipments_internal:
                internal_shipments.extend(list(record.shipments_internal))
            if record.shipments_out:
                out_shipments.extend(list(record.shipments_out))
        internal_shipments = list(set(internal_shipments))
        out_shipments = list(set(out_shipments))
        sales = [s for r in records for s in r.sales]
        if sales:
            sales = Sale.browse(sales)
        # get all vouchers of shipments
        if sales:
            cls.set_sales(records, 'sales',
                [('delete', list(map(int, sales)))],
                check_cancelled=False)
        if internal_shipments or out_shipments:
            with Transaction().set_context(check_origin=False):
                if internal_shipments:
                    cls.set_shipments(records, 'shipments_internal',
                        [('delete', list(map(int, internal_shipments)))],
                        check_cancelled=False)
                    internal_shipments = [
                        s for s in Shipment.browse(internal_shipments)
                        if not s.moves
                    ]
                    if internal_shipments:
                        Shipment.cancel(internal_shipments)
                if out_shipments:
                    cls.set_shipments(records, 'shipments_out',
                        [('delete', list(map(int, out_shipments)))],
                        check_cancelled=False)
                    out_shipments = [
                        s for s in ShipmentOut.browse(out_shipments)
                        if not s.moves
                    ]
                    if out_shipments:
                        ShipmentOut.cancel(out_shipments)
        cls.write(records, {
            'state': 'processing',
            'received_by': None,
            'received_date': None,
        })
        cls._set_planned_date(records, None)

    @classmethod
    @ModelView.button
    def unrecover(cls, records):
        cls.undo(records)

    @classmethod
    @ModelView.button
    @Workflow.transition('processing')
    def process(cls, vouchers):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')

        shipments = []
        for voucher in vouchers:
            if voucher.state in ('processing', 'done'):
                continue

            shipment = voucher._get_internal_shipment(Shipment)
            shipments.append(shipment)

        if shipments:
            Shipment.save(shipments)
            Shipment.wait(shipments)
            Shipment.assign_force(shipments)
            Shipment.ship(shipments)
            Shipment.done(shipments)

        if vouchers:
            cls.save(vouchers)

    @classmethod
    @Workflow.transition('recovery')
    def do(cls, vouchers):
        done = []
        for voucher in vouchers:
            if voucher.is_done():
                if voucher.state != 'done':
                    done.append(voucher)
        if done:
            cls.done(done)

    def is_done(self):
        return (
            not self.shipments_out
            or all(s.state in ('done', 'cancelled')
                for s in self.shipments_out)
            and any(s.state == 'done' for s in self.shipments_out)
            ) and (
            not self.shipments_internal
            or all(s.state in ('done', 'cancelled')
                for s in self.shipments_internal)
            and any(s.state == 'done' for s in self.shipments_internal)
            ) and (
            not self.sales
            or all(s.state in ('processing', 'done', 'cancelled')
                for s in self.sales)
            and any(s.state in ('processing', 'done') for s in self.sales)
        )

    @classmethod
    @Workflow.transition('done')
    def done(cls, vouchers):
        pass

    def _get_internal_shipment(self, Shipment):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        Move = pool.get('stock.move')

        number = None
        if self.receiver_moves:
            shipments = list(set(m.shipment for m in self.receiver_moves))
            number = shipments[-1].number
            moves = [m for shipment in shipments
                for m in shipment.moves]
            Move.draft(moves)
            for move in moves:
                move.origin = None
            Move.save(moves)
            Shipment.delete(shipments)

        from_loc, to_loc = self._get_internal_shipment_locations()
        shipment = Shipment(
            planned_date=self.date,
            planned_start_date=self.date,
            effective_date=self.date,
            effective_start_date=self.date,
            company=self.company,
            from_location=from_loc,
            to_location=to_loc,
            comment=self.get_format(self._get_comment_substitutions()))
        if number:
            shipment.number = number
        move = Move(
            quantity=self.quantity,
            product=self.product,
            uom=self.product.default_uom.id,
            from_location=from_loc,
            to_location=to_loc,
            company=self.company,
            currency=self.company.currency,
            origin=self)
        move.on_change_product()
        shipment.moves = [move]
        return shipment

    def _get_internal_shipment_locations(self):
        _from = self.warehouse.storage_location
        if not _from:
            self.raise_user_error('not_from_location', self.carrier.rec_name)
        _to = (self.issuing_warehouse
            and self.issuing_warehouse.storage_location or None)
        if not _to:
            self.raise_user_error('not_to_location',
                self.issuing_address.rec_name)
        return (_from, _to)

    @classmethod
    @ModelView.button_action('stock_voucher.wizard_done_voucher')
    def done_try(cls, vouchers):
        pass

    def get_format(self, substitutions):
        Conf = Pool().get('stock.configuration')
        full_comment = Template(
            Conf(1).voucher_comment or '').substitute(**substitutions)
        return full_comment

    def _get_comment_substitutions(self):
        return {
            'company_name': getattr(self.company, 'name', '') or '',
            'issued_by_name': getattr(self.issued_by, 'name', ''),
            'number': getattr(self, 'number', '') or '',
            'reference': getattr(self, 'reference', '') or '',
            'date': getattr(self, 'date', '').strftime('%d/%m/%Y')
                if getattr(self, 'date', '') else '',
            'maturity_date': getattr(self, 'maturity_date', '') or '',
            'product_name': getattr(self.product, 'name', '') or '',
            'quantity': getattr(self, 'quantity', '') or '',
            'external_picking': getattr(self, 'external_picking', '') or '',
            'external_picking_date': getattr(self, 'external_picking_date',
                '').strftime('%d/%m/%Y') if getattr(self,
                'external_picking_date', '') else '',
            'carrier_name': getattr(self.carrier, 'name', '') or '',
            'emitter_name': getattr(self.emitter, 'name') or '',
            'managed_by': getattr(self, 'managed_by', '') or '',
        }

    @fields.depends('product')
    def on_change_with_unit_digits(self, name=None):
        if self.product:
            return self.product.default_uom.digits
        return 2

    @fields.depends('issued_by')
    def on_change_issued_by(self, name=None):
        if self.issued_by and len(self.issued_by.addresses) == 1:
            self.issuing_address = self.issued_by.addresses[0]

    @fields.depends('external_picking_date')
    def on_change_with_maturity_date(self, name=None):
        if self.external_picking_date:
            return self.external_picking_date + relativedelta(days=90)

    @fields.depends('product')
    def on_change_with_product_uom_category(self, name=None):
        if self.product:
            return self.product.default_uom_category.id

    @fields.depends('product')
    def on_change_product(self):
        if self.product:
            self.uom = self.product.default_uom
            self.unit_digits = self.product.default_uom.digits

    @classmethod
    def check_unique_restriction(cls, records, fields):
        sorted_records = sorted(records, key=lambda x: (getattr(x, fname, None)
            for fname in fields))
        for key, grouped_records in groupby(sorted_records, key=lambda x: (
                getattr(x, fname, None) for fname in fields)):
            key = list(key)
            if any(not x for x in key):
                return
            message = '\n'.join(['- %s=%s' % (
                cls._translate(f), v.rec_name if isinstance(v, Model) else v)
                for f, v in zip(fields, key)])
            grouped_records = list(grouped_records)
            if len(grouped_records) > 1:
                cls.raise_user_error('validate_unique', message)
            domain = [
                (f, '=', v) for f, v in zip(fields, key)
            ]
            others = cls.search(domain + [
                ('id', 'not in', [r.id for r in grouped_records])])
            if others:
                cls.raise_user_error('validate_unique', message)

    @classmethod
    def _translate(cls, field):
        res = cls.fields_get(fields_names=[field])
        return res[field]['string']

    @classmethod
    def get_count(cls, records, name=None):
        return {r.id: 1 for r in records}

    @classmethod
    def _set_planned_date(cls, vouchers, date, comment=None):
        VoucherPlannedDate = Pool().get('stock.voucher.planned_date')
        planned_dates = []
        for voucher in vouchers:
            if voucher.planned_date:
                planned_dates.append(VoucherPlannedDate(
                    voucher=voucher,
                    date=voucher.planned_date,
                    comment=voucher.comment))
            voucher.planned_date = date
            if date:
                voucher.comment = comment

        if planned_dates:
            VoucherPlannedDate.save(planned_dates)
        cls.save(vouchers)

    @classmethod
    def _get_move_states(cls):
        pool = Pool()
        ShipmentInternal = pool.get('stock.shipment.internal')
        ShipmentOut = pool.get('stock.shipment.out')

        states = ShipmentOut.fields_get(['state'])['state']['selection']
        states.extend(ShipmentInternal.fields_get(['state']
            )['state']['selection'])
        return list(set(states))

    @classmethod
    def _get_shipment_origins(cls):
        Move = Pool().get('stock.move')
        return Move.get_shipment()

    def get_move_data(self, name=None):
        if name.startswith('receiver'):
            moves = self.receiver_moves
        else:
            moves = self.moves
        moves = [m for m in moves if m.state != 'cancel']
        if moves:
            move = moves[0]
            if not move.shipment:
                return None
            if name.endswith('_date'):
                return move.shipment.effective_date
            elif name.endswith('_state'):
                return move.shipment.state
            elif name.endswith('_shipment'):
                return str(move.shipment)


class DoneVoucherStateView(StateView):

    def get_view(self, wizard, state_name):
        with Transaction().set_context(done_voucher=True):
            return super(DoneVoucherStateView, self).get_view(
                wizard, state_name)


class CreateShipment(ModelSQL, ModelView):
    """Create Shipment"""
    __name__ = 'stock.voucher.create_shipment.start'

    received_by = fields.Many2One('party.party', 'Received by',
        required=True)
    received_date = fields.Date('Received Date', required=True)
    shipment_type = fields.Selection([
        ('shipment_internal', 'Shipment Internal'),
        ('shipment_out', 'Shipment Out'),
        ('sale', 'Sale')
        ], 'Shipment type', required=True)
    customer = fields.Many2One('party.party', 'Customer',
        states={
            'invisible': (Eval('shipment_type') != 'shipment_out'),
            'required': (Eval('shipment_type') == 'shipment_out')
        },
        depends=['shipment_type'])
    delivery_address = fields.Many2One('party.address', 'Delivery Address',
        domain=[('party', '=', Eval('customer'))],
        states={
            'invisible': (Eval('shipment_type') != 'shipment_out'),
            'required': (Eval('shipment_type') == 'shipment_out')
        },
        depends=['shipment_type', 'customer'])
    to_location = fields.Many2One('stock.location', 'To Location',
        domain=[('type', 'in', ['view', 'storage', 'lost_found'])],
        states={
            'invisible': Eval('shipment_type') != 'shipment_internal',
            'required': Eval('shipment_type') == 'shipment_internal'},
        depends=['shipment_type'])
    returned = fields.Boolean('Returned to Customer/Emitter')

    @staticmethod
    def default_shipment_type():
        return 'shipment_internal'


class VoucherDone(Wizard):
    '''Voucher Done'''
    __name__ = 'stock.voucher.done'

    start = StateView('stock.voucher.create_shipment.start',
        'stock_voucher.create_shipment_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'shipment', 'tryton-forward', default=True)])
    shipment = StateTransition()
    shipment_internal = DoneVoucherStateView('stock.shipment.internal',
        'stock.shipment_internal_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Done', 'done', 'tryton-ok', default=True)])
    shipment_out = DoneVoucherStateView('stock.shipment.out',
        'stock.shipment_out_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Done', 'done', 'tryton-ok', default=True)])
    sale = ModifyHeaderStateView('sale.sale', 'sale.sale_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'sale_moves', 'tryton-forward', default=True)])
    sale_moves = DoneVoucherStateView('stock.shipment.out',
        'stock_voucher.shipment_out_outgoing_move_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Done', 'done', 'tryton-ok', default=True)])
    done = StateTransition()

    @classmethod
    def __setup__(cls):
        super(VoucherDone, cls).__setup__()
        cls._error_messages.update({
            'not_same_issuer': ('Vouchers selected must have the same '
                'issuer.'),
            'invalid_moves': ('The sum of moves quantity ("%s") '
                'must match the sum of vouchers quantity ("%s").'),
            'done_voucher_unmatch':
                'Vouchers must match on fields "Issued by", "Issuing address" '
                'and "Emitter".'
        })

    def default_start(self, fields):
        vouchers = self._get_vouchers()
        keys = set()
        for voucher in vouchers:
            keys.add((voucher.issued_by.id,
                voucher.issuing_address.id if voucher.issuing_address else None,
                voucher.emitter.id))
            if len(keys) > 1:
                self.raise_user_error('done_voucher_unmatch')
        return {}

    def transition_shipment(self):
        return self.start.shipment_type

    def _get_vouchers(self):
        pool = Pool()
        Voucher = pool.get('stock.voucher')
        return Voucher.browse(Transaction().context['active_ids'])

    def _get_shipment_comment(self, vouchers):
        comment = []
        for voucher in vouchers:
            voucher_comment = voucher.get_format(
                voucher._get_comment_substitutions())
            if voucher_comment:
                comment.append(voucher_comment)
        return '\n'.join(comment) or None

    def default_shipment_internal(self, fields):
        vouchers = self._get_vouchers()
        locations = set([v._get_internal_shipment_locations()[1]
            for v in vouchers])

        if len(locations) > 1:
            self.raise_user_error('not_same_issuer')

        from_loc, = locations
        moves = self._get_moves(from_loc,
            self.start.to_location)
        res = {
            'from_location': from_loc.id,
            'to_location': self.start.to_location.id,
            'moves': moves,
            'comment': self._get_shipment_comment(vouchers)
        }
        return res

    def default_shipment_out(self, fields):
        vouchers = self._get_vouchers()
        issued_address = set(
            (voucher.issuing_address, voucher.issuing_warehouse)
            for voucher in vouchers)
        if len(issued_address) > 1:
            self.raise_user_error('not_same_issuer')

        issued_address = issued_address.pop()
        moves = self._get_moves(issued_address[1].output_location, None)
        res = {
            'customer': self.start.customer.id,
            'delivery_address': self.start.delivery_address.id,
            'warehouse': issued_address[1].id,
            'outgoing_moves': moves,
            'comment': self._get_shipment_comment(vouchers)
        }
        return res

    def default_sale(self, fields):
        vouchers = self._get_vouchers()
        issued_address = set(
            (voucher.issuing_address, voucher.issuing_warehouse)
            for voucher in vouchers)
        if len(issued_address) > 1:
            self.raise_user_error('not_same_issuer')

        issued_address = issued_address.pop()
        res = {
            'warehouse': issued_address[1].id,
            'comment': self._get_shipment_comment(vouchers),
            'sale_date': self.start.received_date,
            'shipment_method': 'manual'
        }
        return res

    def default_sale_moves(self, fields):
        moves = self._get_moves(self.sale.warehouse.output_location,
            self.sale.party.customer_location, with_vouchers=True)
        return {
            'company': self.sale.company.id,
            'customer': self.sale.party.id,
            'delivery_address': self.sale.shipment_address.id,
            'warehouse': self.sale.warehouse.id,
            'outgoing_moves': moves,
            'effective_date': self.start.received_date
        }

    def _get_moves(self, from_location, to_location, with_vouchers=False):
        vouchers = self._get_vouchers()
        new_moves = []
        for voucher in vouchers:
            moves = self._get_voucher_moves([voucher])
            for product, product_moves in groupby(moves,
                    key=lambda m: m.product):
                move = {
                    'product': product.id,
                    'uom': product.default_uom.id,
                    'quantity': sum(m.internal_quantity
                        for m in product_moves if m.internal_quantity),
                    'from_location':
                        from_location.id if from_location else None,
                    'to_location': to_location.id if to_location else None,
                    'company': voucher.company.id,
                    'state': 'draft',
                    'vouchers': [] if not with_vouchers else [voucher.id],
                    'effective_date': self.start.received_date,
                    'origin': '%s,%s' % (voucher.__name__, voucher.id)
                        if not with_vouchers else None
                }
            new_moves.append(move)
        return new_moves

    def _get_voucher_moves(self, vouchers=None):
        if not vouchers:
            vouchers = self._get_vouchers()
        return sorted([move for voucher in vouchers
            for move in voucher.receiver_moves if move.state != 'cancelled'],
            key=lambda m: m.product)

    def transition_done(self):
        pool = Pool()
        Voucher = pool.get('stock.voucher')
        Move = pool.get('stock.move')
        Shipment = pool.get('stock.shipment.internal'
            if self.start.shipment_type == 'shipment_internal'
            else 'stock.shipment.out')
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')

        vouchers = self._get_vouchers()
        shipments = []
        Voucher.write(vouchers, {'returned': self.start.returned})
        for voucher in vouchers:
            shipments.extend(voucher.shipments_internal
                if Shipment.__name__ == 'stock.shipment.internal'
                else voucher.shipments_out)
        number, sale_number = None, None
        date = self.start.received_date
        if shipments:
            shipments = list(set(shipments))
            number = shipments[-1].number
            moves = [m for shipment in shipments
                for m in shipment.moves
            ]
            Move.draft(moves)
            for move in moves:
                move.origin = None
            Move.save(moves)
            Shipment.delete(shipments)
        if vouchers[0].sales:
            sale_number = vouchers[0].sales[-1].number
            Sale.delete(vouchers[0].sales)

        shipment = None
        if self.start.shipment_type == 'shipment_internal':
            shipment = self.shipment_internal
        elif self.start.shipment_type == 'shipment_out':
            shipment = self.shipment_out
        elif self.start.shipment_type == 'sale':
            shipment = self._get_shipment_from_sale()
            date = None

        moves = shipment.moves if self.start.shipment_type == \
            'shipment_internal' else shipment.outgoing_moves

        total_voucher_qty = sum([v.quantity for v in vouchers])
        moves_quantity = sum(move.quantity for move in moves)
        if moves_quantity != total_voucher_qty:
            self.raise_user_error('invalid_moves', (moves_quantity,
                total_voucher_qty))

        pending_vouchers = {v.id: v.quantity for v in vouchers}
        move_qties = {m.id: m.quantity for m in moves}
        for i, move in enumerate(moves * 2):
            if move_qties[move.id] <= 0:
                continue
            for voucher in vouchers:
                if voucher.id not in pending_vouchers:
                    continue
                if voucher.product.id == move.product.id or (i / 3) > 0:
                    qty = min(move.uom.compute_qty(voucher.uom,
                        pending_vouchers[voucher.id], move.uom),
                        move_qties[move.id])
                    move_qties[move.id] -= qty
                    pending_vouchers[voucher.id] -= voucher.uom.compute_qty(
                        move.uom, qty, voucher.uom)
                    if pending_vouchers[voucher.id] <= 0:
                        pending_vouchers.pop(voucher.id)
                if move_qties[move.id] <= 0:
                    break

        assert not pending_vouchers
        assert sum(value for value in list(move_qties.values())) <= 0

        shipment.number = number
        if self.start.shipment_type == 'shipment_internal':
            shipment.planned_start_date = shipment.planned_date
            shipment.effective_start_date = shipment.effective_start_date
        shipment.save()

        if self.start.shipment_type == 'sale':
            sale = self.sale
            lines = []
            for move in shipment.outgoing_moves:
                line = SaleLine(
                    sale=sale,
                    type='line',
                    product=move.product,
                    unit=move.uom,
                    quantity=move.quantity,
                    unit_price=move.unit_price,
                    moves=[move])
                line.on_change_product()
                lines.append(line)
                move.unit_price = line.unit_price
            Move.save(shipment.outgoing_moves)
            sale.lines = lines
            sale.number = sale_number
            sale.sale_date = date
            sale.shipment_method = 'manual'
            sale.save()
            if not sale.number:
                Sale.set_number([sale])
            sale.set_shipment_state()

        Voucher.write(vouchers, {
            'received_by': self.start.received_by,
            'received_date': self.start.received_date
            })
        Voucher.do(vouchers)
        return 'end'

    def _get_shipment_from_sale(self):
        Shipment = Pool().get('stock.shipment.out')
        return Shipment(
            company=self.sale.company,
            customer=self.sale.party,
            delivery_address=self.sale.shipment_address,
            warehouse=self.sale.warehouse,
            comment=self.sale.comment,
            outgoing_moves=self.sale_moves.outgoing_moves,
            effective_date=self.start.received_date
        )


class ModifyHolderStart(ModelView):
    '''Voucher Change Holder Start'''
    __name__ = 'stock.voucher.holder.modify_holder.start'

    holder = fields.Many2One('party.party', 'Holder', required=True)


class VoucherModifyHolder(Wizard):
    '''Voucher Change Holder'''
    __name__ = 'stock.voucher.holder.modify_holder'

    start = StateView('stock.voucher.holder.modify_holder.start',
        'stock_voucher.modify_holder_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
        Button('Modify holder', 'modify_holder', 'tryton-ok', default=True)])
    modify_holder = StateTransition()
    print_ = StateReport('stock.voucher.modify_holder.report')

    def transition_modify_holder(self):
        pool = Pool()
        Holder = pool.get('stock.voucher.holder')
        Voucher = pool.get('stock.voucher')

        vouchers = Voucher.browse(Transaction().context['active_ids'])
        vouchers = [voucher for voucher in vouchers
            if not voucher.holders or voucher.holders[-1] != self.start.holder]
        holders = [Holder(
            holder=self.start.holder,
            voucher=voucher) for voucher in vouchers]
        if holders:
            Holder.save(holders)
        return 'print_'

    def do_print_(self, action):
        return action, {
            'ids': Transaction().context.get('active_ids', None),
        }


class VoucherHolder(ModelSQL, ModelView):
    '''Voucher Holder'''
    __name__ = 'stock.voucher.holder'

    voucher = fields.Many2One('stock.voucher', 'Voucher',
        required=True, select=True, readonly=True)
    holder = fields.Many2One('party.party', 'Holder',
        required=True, select=True, readonly=True)
    date = fields.Date('Date', required=True)

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()

        tableh = cls.__table_handler__(module_name)
        date_exists = tableh.column_exist('date')

        super().__register__(module_name)

        if not date_exists:
            date_type = cls.date.sql_type().base
            cursor.execute(*sql_table.update(
                [sql_table.date], [sql_table.create_date.cast(date_type)],
                where=sql_table.date == Null))

    @classmethod
    def create(cls, vlist):
        today = Pool().get('ir.date').today()
        for vals in vlist:
            if 'date' not in vals:
                vals['date'] = today
        return super().create(vlist)


class VoucherMove(ModelSQL):
    """Voucher Move"""
    __name__ = 'stock.voucher-stock.move'
    _table = 'stock_voucher_move_rel'

    move = fields.Many2One('stock.move', 'Move', ondelete='CASCADE')
    voucher = fields.Many2One('stock.voucher', 'Voucher', ondelete='CASCADE')


class VoucherNote(CompanyReport):
    '''Stock Voucher Report '''
    __name__ = 'stock.voucher'


class VoucherPlannedDate(ModelSQL, ModelView):
    '''Voucher Planned Date'''
    __name__ = 'stock.voucher.planned_date'

    voucher = fields.Many2One('stock.voucher', 'Voucher',
        required=True, select=True, ondelete='CASCADE')
    date = fields.Date('Date', required=True)
    comment = fields.Text('Comment')


class DefinePlannedDateStart(ModelView):
    '''Voucher Define Planned Date Start'''
    __name__ = 'stock.voucher.planned_date.define.start'

    date = fields.Date('Date', required=True)
    comment = fields.Text('Comment')


class DefinePlannedDate(Wizard):
    '''Voucher Define Planned Date'''
    __name__ = 'stock.voucher.planned_date.define'

    start = StateView('stock.voucher.planned_date.define.start',
        'stock_voucher.define_planned_date_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
        Button('Define', 'define', 'tryton-ok', default=True)])
    define = StateTransition()

    def transition_define(self):
        pool = Pool()
        Voucher = pool.get('stock.voucher')

        vouchers = Voucher.browse(Transaction().context['active_ids'])
        Voucher._set_planned_date(vouchers, self.start.date,
            self.start.comment)
        return 'end'
