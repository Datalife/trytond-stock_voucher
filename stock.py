# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from functools import wraps
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id
from trytond.transaction import Transaction

__all__ = ['Configuration', 'ConfigurationSequence', 'ShipmentInternal',
    'Move', 'ShipmentOut']


def do_voucher(func):
    @wraps(func)
    def wrapper(cls, shipments):
        pool = Pool()
        Voucher = pool.get('stock.voucher')
        with Transaction().set_context(_check_access=False):
            vouchers = Voucher.browse(set([
                v for s in cls.browse(shipments)
                for v in s._get_vouchers_to_do()]))
        func(cls, shipments)
        with Transaction().set_context(_check_access=False):
            Voucher.done(vouchers)
    return wrapper


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    voucher_product = fields.Many2One('product.product', 'Voucher product')
    voucher_comment = fields.Text('Voucher comment',
        help='Text to copy on voucher shipments. Allowed values are:\n'
            '- company_name\n'
            '- issued_by_name\n'
            '- number\n'
            '- reference\n'
            '- date\n'
            '- maturity_date\n'
            '- product_name\n'
            '- quantity\n'
            '- external_picking\n'
            '- external_picking_date\n'
            '- carrier_name\n'
            '- emitter_name\n'
            '- managed_by')
    voucher_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', "Voucher Sequence", required=True,
            domain=[
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None]),
                ('sequence_type', '=', Id('stock_voucher',
                    'sequence_type_voucher')),
            ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'voucher_sequence':
            return pool.get('stock.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_voucher_sequence(cls, **pattern):
        return cls.multivalue_model(
            'voucher_sequence').default_voucher_sequence()


class ConfigurationSequence(metaclass=PoolMeta):
    __name__ = 'stock.configuration.sequence'

    voucher_sequence = fields.Many2One(
        'ir.sequence', "Voucher Sequence", required=True,
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('stock_voucher',
                    'sequence_type_voucher')),
        ])

    @classmethod
    def default_voucher_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('stock_voucher', 'sequence_voucher')
        except KeyError:
            return None


class VoucherShipmentMixin(object):

    voucher = fields.Function(fields.Boolean('Voucher'),
        'get_voucher', searcher='search_voucher')

    def get_voucher(self, name=None):
        for move in self.moves:
            if move.voucher:
                return True
        return False

    @classmethod
    def search_voucher(cls, name, clause):
        return [('moves.voucher',) + tuple(clause[1:])]

    @classmethod
    def view_attributes(cls):
        res = super(VoucherShipmentMixin, cls).view_attributes()
        if Transaction().context.get('done_voucher'):
            res.append(('//group[@id="buttons"]', 'states',
                {'invisible': True}))
        return res

    def _get_vouchers_to_do(self):
        Voucher = Pool().get('stock.voucher')
        vouchers = set()
        for move in self.moves:
            if (move.origin
                    and isinstance(move.origin, Voucher)
                    and move not in move.origin.receiver_moves):
                vouchers.add(move.origin)
            if move.vouchers:
                vouchers |= set(move.vouchers)
        return list(vouchers)


class ShipmentInternal(VoucherShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    @classmethod
    def __setup__(cls):
        super(ShipmentInternal, cls).__setup__()
        cls.from_location.domain = ['OR',
            cls.from_location.domain,
            ('type', '=', 'supplier'),
            ]

    @fields.depends('to_location', 'moves')
    def on_change_to_location(self, name=None):
        for move in self.moves:
            move.to_location = self.to_location

    @classmethod
    @do_voucher
    def done(cls, records):
        super(ShipmentInternal, cls).done(records)


class ShipmentOut(VoucherShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    @classmethod
    @do_voucher
    def done(cls, records):
        super(ShipmentOut, cls).done(records)


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    voucher = fields.Function(fields.Boolean('Voucher'),
        'get_voucher', searcher='search_voucher')
    vouchers = fields.Many2Many('stock.voucher-stock.move', 'move',
        'voucher', 'Vouchers', readonly=True)

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + ['stock.voucher']

    def get_voucher(self, name=None):
        return self.origin and self.origin.__name__ == 'stock.voucher' or \
            bool(self.vouchers)

    @classmethod
    def search_voucher(cls, name, clause):
        operator = {
            '=': 'like' if clause[2] else 'not like',
            '!=': 'not like' if clause[2] else 'like'
        }
        reverse_clause = {
            '=': '!=',
            '!=': '='
        }
        if clause[2]:
            join_op = 'OR' if clause[1] == '=' else 'AND'
            origin_op = 'AND' if clause[1] == '=' else 'OR'
            domain = [join_op,
                [origin_op,
                    ('origin', reverse_clause[clause[1]], None),
                    ('origin', operator[clause[1]], 'stock.voucher,%')],
                ('vouchers', reverse_clause[clause[1]], None)]
        else:
            join_op = 'AND' if clause[1] == '=' else 'OR'
            origin_op = 'OR' if clause[1] == '=' else 'AND'
            domain = [join_op,
                [origin_op,
                    ('origin', clause[1], None),
                    ('origin', operator[clause[1]], 'stock.voucher,%')],
                ('vouchers', clause[1], None)]
        return domain
